# Advem Studio

Adam Drabik's portfolio created in Gatsby with React. Website is generated statically using dynamic content from outsite data store. Presents authors programming knowledge, mastered technologies and libraries and most of major projects. Built-in two languages set.

## Visit

http://localhost:8000

## Technologies

- Gatsby, GraphQL, React, Sass, Netlify, Adobe Illustrator

## Features

- CMS (Content Management System) provided by Contentful - data is fetched using query language GraphQL
- Two built-in scalable language sets (English and Polish) which changes content dynamically based on app state
- Webpage sections rendered based on list - uses React's dynamic component naming (sections can be controled and structured with object hierarchy)
- Custom themes changes appearance based on app state
- Neatly created elements layout and interface styles using Sass preprocessor
- Copyright graphics

Copyright (C) 2020 Adam 'Advem' Drabik
