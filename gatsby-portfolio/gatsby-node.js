const path = require('path')

module.exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const projectTemplate = path.resolve(`./src/templates/Project.js`)

  const res = await graphql(`
    query {
      allContentfulProject(filter: { node_locale: { eq: "en-US" } }) {
        edges {
          node {
            slug
          }
        }
      }
    }
  `)

  res.data.allContentfulProject.edges.forEach(edge => {
    createPage({
      component: projectTemplate,
      path: `/projects/${edge.node.slug}`,
      context: {
        slug: edge.node.slug,
      },
    })
  })
}
