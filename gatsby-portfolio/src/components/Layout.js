import React, { useContext } from 'react'
import { StateContext } from '../context/ContextProvider'
import Header from '../components/Header'
import s from '../styles/layout.module.scss'
import { theme, themeBackground } from '../context/theme'

const Layout = props => {
  const state = useContext(StateContext)
  const style = {
    backgroundColor: `rgba(${themeBackground(state.theme)}, 1)`,
    color: `rgba(${theme(state.theme)}, 1)`,
  }

  return (
    <div className={s.main} css={style}>
      <Header />
      <div>{props.children}</div>
    </div>
  )
}

export default Layout
