import React from 'react'
import { Helmet } from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'

const Head = props => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
          author
          description
          image
          url
          icon
        }
      }
    }
  `)
  return (
    <Helmet title={`${props.title} | ${data.site.siteMetadata.title}`}>
      <meta
        name="description"
        content={props.description || data.site.siteMetadata.description}
      />
      <meta name="author" content={data.site.siteMetadata.author} />
      <meta name="theme-color" content="#171717" />
      <meta
        property="og:title"
        content={`${props.title} | ${data.site.siteMetadata.title}`}
      />
      <meta
        property="og:description"
        content={props.description || data.site.siteMetadata.description}
      />
      <meta
        property="og:image"
        content={
          props.image ||
          `${data.site.siteMetadata.url}${data.site.siteMetadata.image}`
        }
      />
      <link
        rel="icon"
        sizes="192x192"
        href={
          props.icon ||
          `${data.site.siteMetadata.url}${data.site.siteMetadata.icon}`
        }
      />
      <meta
        property="og:url"
        content={
          props.url
            ? `${data.site.siteMetadata.url}${props.url}`
            : data.site.siteMetadata.url
        }
      />
      <meta property="og:type" content="website" />
      <meta name="robots" content="index, follow" />
      <link
        rel="preload"
        href="/fonts/OpenSans-Light.ttf"
        as="font"
        type="font/ttf"
        crossorigin
      />
      <link
        rel="preload"
        href="/fonts/OpenSans-Regular.ttf"
        as="font"
        type="font/ttf"
        crossorigin
      />
      <link
        rel="preload"
        href="/fonts/OpenSans-Bold.ttf"
        as="font"
        type="font/ttf"
        crossorigin
      />
    </Helmet>
  )
}

export default Head
