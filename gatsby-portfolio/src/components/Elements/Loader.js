import React from 'react'
import s from '../../styles/loader.module.scss'

/*
 add style property to parent:
    position: relative;
*/

const Loader = () => (
  <div className={s.inTheCenter}>
    <div className={s.ldsRing}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
)
export default Loader
