import React from 'react'
import { Link } from 'gatsby'
import s from '../../styles/header.module.scss'
import logo from '../../assets/icons/AdvemStudioLogo'

const Logo = () => {
  return (
    <Link to={'/#Home'} className={s.logo}>
      <div className={s.icon}>{logo}</div>
      <h1 className={s.name}>
        <span className={s.Advem}>Advem</span>
        <span className={s.Studio}>Studio</span>
      </h1>
    </Link>
  )
}

export default Logo
