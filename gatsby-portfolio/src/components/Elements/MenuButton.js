import React, { useState, useContext } from 'react'
// import scrollTo from 'gatsby-plugin-smoothscroll'
import { Link } from 'gatsby'
import { DispatchContext, StateContext } from '../../context/ContextProvider'
import { theme } from '../../context/theme'
import s from '../../styles/menu.module.scss'

const MenuButton = props => {
  const [active, setActive] = useState(false)
  const dispatch = useContext(DispatchContext)
  const state = useContext(StateContext)

  const changeState = () => {
    if (props.name === 'theme') dispatch({ type: 'TOGGLE_THEME' })
    if (props.name === 'language') dispatch({ type: 'TOGGLE_LANGUAGE' })
  }

  const styleContainerStatic = active
    ? `${s[`container${props.name}`]} ${s[`hovered${props.name}`]}`
    : `${s[`container${props.name}`]}`

  const styleContainerDynamic = active
    ? { backgroundColor: `rgba(${theme(state.theme)}, 0.1)` }
    : { backgroundColor: 'transparent' }

  const styleTheme = `rgb(${theme(state.theme)})`

  return (
    <div
      className={styleContainerStatic}
      css={styleContainerDynamic}
      onMouseEnter={() => setActive(true)}
      onMouseLeave={() => setActive(false)}
      onKeyPress={() => changeState()}
      onClick={() => changeState()}
      role="menuitem"
      tabIndex="0"
    >
      {active && (
        <div className={`${s.textField} ${s[`${props.name}Text`]}`}>
          {props.data.map((item, id) => (
            <div className={s.text} key={id} css={{ color: styleTheme }}>
              {props.name === 'menu' ? (
                // <div onClick={() => scrollTo(`#${item.component}`)}>
                //   {item.name}
                // </div>
                <Link to={`/#${item.component}`}>{item.name}</Link>
              ) : (
                item
              )}
            </div>
          ))}
        </div>
      )}
      <div
        className={active ? `${s.icon} ${s[`${props.name}Active`]}` : s.icon}
      >
        <div
          className={s.icon}
          css={{ fill: styleTheme, '> div': { backgroundColor: styleTheme } }}
        >
          {props.icon}
        </div>
      </div>
    </div>
  )
}

export default MenuButton
