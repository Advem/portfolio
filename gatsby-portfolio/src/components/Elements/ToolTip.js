import React from 'react'
import s from '../../styles/tooltip.module.scss'

const ToolTip = props => {
  return <div className={s[`tooltip${props.type}`]}>{props.text}</div>
}

export default ToolTip
