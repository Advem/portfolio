import React, { useContext } from 'react'
// import scrollTo from 'gatsby-plugin-smoothscroll'
import { Link } from 'gatsby'
import { StateContext } from '../../context/ContextProvider'
import { text } from '../../context/langauges'
import { themeBackground, theme } from '../../context/theme'
import s from '../../styles/mobileMenu.module.scss'

const MenuMobile = () => {
  const state = useContext(StateContext)
  const styleContainerStatic = state.menuOn
    ? `${s.containerActive} ${s.container}`
    : s.container

  const styleContainerDynamic = [
    {
      backgroundColor: `rgba(${themeBackground(state.theme)}, 0.95)`,
    },

    state.menuOn
      ? { color: `rgb(${theme(state.theme)})` }
      : { color: 'transparent' },
  ]

  return (
    <div className={styleContainerStatic} css={styleContainerDynamic}>
      <div className={s.itemsContainer}>
        {state.menuOn &&
          text[state.language].menu.map(item => (
            <Link className={s.item} key={item.name} to={`/#${item.component}`}>
              .{item.name}
            </Link>
          ))}
      </div>
    </div>
  )
}

export default MenuMobile
