import React, { useContext, useState } from 'react'
import { Link } from 'gatsby'
import { StateContext } from '../../context/ContextProvider'
import { text } from '../../context/langauges'

import Img from 'gatsby-image'

import s from '../../styles/projectSmall.module.scss'
import a from '../../styles/animations.module.scss'

const ProjectSmall = props => {
  const state = useContext(StateContext)
  const [hovered, setHovered] = useState(false)

  const hoveredColor = hovered
    ? {
        background: `${props.data.color}80, black`,
      }
    : { background: `${props.data.color}00` }

  const infoStyle = {
    background: `linear-gradient(-45deg, ${props.data.color}26, ${props.data.color})`,
  }

  const renderLogo = () =>
    props.data.logo.fixed ? (
      <Img fixed={props.data.logo.fixed} alt={props.data.logo.title} />
    ) : (
      <img
        className={s.logoImage}
        src={props.data.logo.file.url}
        alt={props.data.logo.title}
      />
    )

  return (
    <div
      className={
        props.isvisible || hovered
          ? `${s.projectContainer} ${a.project}`
          : `${s.projectContainer} ${a.dim}`
      }
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      onClick={() => setHovered(!hovered)}
      onKeyPress={() => setHovered(!hovered)}
      role="button"
      tabIndex="0"
    >
      <div className={s.infoContainer} css={infoStyle}>
        <div className={s.text}>
          <div>
            <h4>{props.data.title}</h4>
            <h5>{props.data.subtitle}</h5>
          </div>
          {renderLogo()}
        </div>
        <p>{props.data.phrase}</p>
        <Link to={`/projects/${props.data.slug}`} className={s.button}>
          {text[state.language].explore}
        </Link>
      </div>
      <div className={s.coverContainer}>
        {props.data.cover && (
          <Img fluid={props.data.cover.fluid} alt={props.data.cover.title} />
        )}
        <div
          className={
            hovered
              ? `${s.descriptionContainer} ${s.active}`
              : s.descriptionContainer
          }
          css={hoveredColor}
        >
          <div className={s.descriptionTop}>
            {props.data.shortDescription && (
              <p>{props.data.shortDescription.shortDescription}</p>
            )}
            <div className={s.logoContainer}>{renderLogo()}</div>
          </div>
          <div className={s.descriptionBot}>{props.data.type}</div>
        </div>
      </div>
    </div>
  )
}

export default ProjectSmall
