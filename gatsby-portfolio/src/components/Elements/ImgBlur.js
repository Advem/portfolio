import React, { useState, useEffect } from 'react'
import Loader from '../Elements/Loader'

const preloadPromise = src =>
  new Promise((res, rej) => {
    const image = new Image()
    image.onload = () => res()
    image.src = src
  })

const ImgBlur = props => {
  const [loaded, setLoaded] = useState(false)

  const loadImage = async () => {
    await preloadPromise(props.url)
    setLoaded(true)
  }

  useEffect(() => {
    if (!loaded) loadImage()
  })

  return (
    <>
      {!loaded && <Loader />}
      <img src={loaded ? props.url : props.base64} alt={props.alt} />
    </>
  )
}

export default ImgBlur
