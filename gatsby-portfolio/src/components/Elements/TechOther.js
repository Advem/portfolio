import React, { useState } from 'react'
import Tooltip from '../Elements/ToolTip'
import s from '../../styles/abilities.module.scss'

const TechOther = props => {
  const [hovered, setHovered] = useState(false)
  return (
    <div
      className={s.otherTech}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      onClick={() => setHovered(true)}
      onKeyPress={() => setHovered(!hovered)}
      role="button"
      tabIndex="0"
    >
      {props.icon}
      {hovered && <Tooltip text={props.name} type="bottom" />}
    </div>
  )
}

export default TechOther
