import React, { useContext } from 'react'
import { DispatchContext, StateContext } from '../../context/ContextProvider'
import { theme } from '../../context/theme'
import s from '../../styles/menu.module.scss'

const BurgerButton = props => {
  const dispatch = useContext(DispatchContext)
  const state = useContext(StateContext)

  const styleContainerStatic = state.menuOn
    ? `${s.containerMobile} ${s[`hovered${props.name}`]}`
    : `${s.containerMobile}`

  const styleContainerDynamic = state.menuOn
    ? { backgroundColor: `rgba(${theme(state.theme)}, 0.1)` }
    : { backgroundColor: 'transparent' }

  const styleTheme = `rgb(${theme(state.theme)})`
  const styleIcon = state.menuOn
    ? `${s.icon} ${s[`${props.name}Active`]}`
    : s.icon

  return (
    <div
      className={styleContainerStatic}
      css={styleContainerDynamic}
      onClick={() => dispatch({ type: 'TOGGLE_MENU' })}
      onMouseLeave={() => state.menuOn && dispatch({ type: 'TOGGLE_MENU' })}
      onKeyPress={() => null}
      role="menu"
      tabIndex="0"
    >
      {props.children}
      <div className={styleIcon}>
        <div
          className={s.icon}
          css={{
            fill: styleTheme,
            '> div': { backgroundColor: styleTheme },
          }}
        >
          {props.icon}
        </div>
      </div>
    </div>
  )
}

export default BurgerButton
