import React, { useContext, useEffect, useState } from 'react'
import { StateContext } from '../../context/ContextProvider'
import { theme } from '../../context/theme'
import { text } from '../../context/langauges'
import s from '../../styles/abilities.module.scss'
import a from '../../styles/animations.module.scss'

const Tech = props => {
  const state = useContext(StateContext)
  const [levelColor, setLevelColor] = useState('black')
  const [levelTitle, setLevelTitle] = useState('Loading...')

  const techLevelTheme = {
    backgroundColor: `rgba(${theme(state.theme)}, 0.1)`,
  }

  const techLevelColor = {
    opacity: props.isvisible ? '1' : '0',
    background: `linear-gradient(90deg, ${levelColor}40, ${levelColor})`,
  }

  const techLevelWidth = {
    transitionDuration: '1.5s',
    width: `${props.isvisible ? `${props.level}%` : `0px`}`,
  }

  useEffect(() => {
    if (props.level <= 20) {
      setLevelColor('#FF1100')
      setLevelTitle(text[state.language].level[0])
    } else if (props.level > 20 && props.level <= 40) {
      setLevelColor('#FFBB00')
      setLevelTitle(text[state.language].level[1])
    } else if (props.level > 40 && props.level <= 60) {
      setLevelColor('#00D602')
      setLevelTitle(text[state.language].level[2])
    } else if (props.level > 60 && props.level <= 80) {
      setLevelColor('#00A1FF')
      setLevelTitle(text[state.language].level[3])
    } else if (props.level > 80 && props.level <= 100) {
      setLevelColor('#D300FE')
      setLevelTitle(text[state.language].level[4])
    }
  }, [props.level, state.language, setLevelColor, setLevelTitle])

  return (
    <div
      className={
        props.isvisible
          ? `${s.techContainer} ${s.isVisible}`
          : `${s.techContainer} ${a.hide}`
      }
    >
      <div className={s.techIcon}>{props.icon}</div>
      <div className={s.techRightContainer}>
        <div className={s.techTextContainer}>
          <span className={s.techTextName}>
            {props.name} {props.number}
          </span>
          <span className={s.techTextLevel}>{levelTitle}</span>
        </div>
        <div className={s.techLevelContainer}>
          <div className={s.techLevelBack} css={techLevelTheme}></div>
          <div
            className={s.techLevelFront}
            css={[techLevelWidth, techLevelColor]}
          ></div>
        </div>
      </div>
    </div>
  )
}

export default Tech
