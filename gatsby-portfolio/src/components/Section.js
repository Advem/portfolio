import React from 'react'
import s from '../styles/section.module.scss'

const Section = props => (
  <section className={s.section} id={props.id}>
    {props.children}
  </section>
)

export default Section
