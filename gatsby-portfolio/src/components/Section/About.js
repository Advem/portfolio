import React, { useContext } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'
import { StateContext } from '../../context/ContextProvider'
import { text } from '../../context/langauges'
import s from '../../styles/about.module.scss'
import a from '../../styles/animations.module.scss'

const About = props => {
  const state = useContext(StateContext)

  const styleTheme = {
    // backgroundColor: `rgba(${themeBackground(state.theme, true)}, 0.05)`,
  }

  const image = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "assets/graphics/avatarAbout.png" }) {
        name
        childImageSharp {
          fixed(jpegProgressive: true, width: 120) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return (
    <div
      className={
        props.isvisible
          ? `${s.container} ${a.about}`
          : `${s.container} ${a.hide}`
      }
    >
      <h3>
        {text[state.language].hello}!<p>{text[state.language].hellosub}</p>
      </h3>
      <div className={s.info} css={styleTheme}>
        <div className={s.topContainer}>
          <h5>
            <div>
              <span>Adam</span>
              <span>Drabik</span>
            </div>
            <span>Developer & Designer</span>
          </h5>
          <div className={s.photoContainer}>
            <Img
              fixed={image.file.childImageSharp.fixed}
              alt={image.file.name}
            />
          </div>
        </div>
        <div className={s.text}>
          {text[state.language].about.map(({ title, text }, id) => (
            <div key={id}>
              {title && <p className={s.headline}>{title}</p>}
              {text && <p className={s.content}>{text}</p>}
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default About
