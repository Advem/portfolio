import React, { useContext } from 'react'
import { useStaticQuery, graphql as graph } from 'gatsby'
import Tech from '../Elements/Tech'
import TechOther from '../Elements/TechOther'
import { StateContext } from '../../context/ContextProvider'
import { text } from '../../context/langauges'
import s from '../../styles/abilities.module.scss'
import a from '../../styles/animations.module.scss'

import VisibilitySensor from 'react-visibility-sensor'

import adobeillustrator from '../../assets/icons/adobeillustrator'
import adobephotoshop from '../../assets/icons/adobephotoshop'
import adobexd from '../../assets/icons/adobexd'
import css from '../../assets/icons/css'
import englishc1 from '../../assets/icons/englishc1'
import gatsby from '../../assets/icons/gatsby'
import graphql from '../../assets/icons/graphql'
import html5 from '../../assets/icons/html5'
import javascript from '../../assets/icons/javascript'
import jest from '../../assets/icons/jest'
import nodejs from '../../assets/icons/nodejs'
import polishnative from '../../assets/icons/polishnative'
import react from '../../assets/icons/react'
import redux from '../../assets/icons/redux'
import sass from '../../assets/icons/sass'
import socketio from '../../assets/icons/socketio'

import unknown from '../../assets/icons/unknown'

import webpack from '../../assets/icons/webpack'
import git from '../../assets/icons/git'
import c from '../../assets/icons/c'
import php from '../../assets/icons/php'
import npm from '../../assets/icons/npm'
import laravel from '../../assets/icons/laravel'
import mysql from '../../assets/icons/mysql'
import aws from '../../assets/icons/aws'

const Icons = {
  adobeillustrator,
  adobephotoshop,
  adobexd,
  css,
  englishc1,
  gatsby,
  graphql,
  html5,
  javascript,
  jest,
  nodejs,
  polishnative,
  react,
  redux,
  sass,
  socketio,
  unknown,
}

const OtherIcons = {
  webpack,
  git,
  c,
  php,
  npm,
  laravel,
  mysql,
  aws,
}

const simpleName = name => name.replace(/[\s+]/g, '').toLocaleLowerCase()

const Abilities = props => {
  const data = useStaticQuery(graph`
  query { allContentfulJsoNs { nodes { technologies {iid, name, level} otherTechnologies {iid, name}}}}`)

  const state = useContext(StateContext)
  const technologies = data.allContentfulJsoNs.nodes[0].technologies
  const other = data.allContentfulJsoNs.nodes[0].otherTechnologies
  return (
    <div className={s.container}>
      <h2 className={s.sectionHeadline}>{props.name}</h2>
      {technologies
        .sort((a, b) => a.iid - b.iid || a.name.localeCompare(b.name))
        .map(tech => (
          <VisibilitySensor
            key={`${tech.iid}${simpleName(tech.name)}`}
            offset={{ top: 20 }}
          >
            {({ isVisible }) => (
              <Tech
                name={tech.name}
                level={tech.level}
                icon={
                  Icons[simpleName(tech.name)]
                    ? Icons[simpleName(tech.name)]
                    : unknown
                }
                key={tech.iid}
                isvisible={isVisible}
              />
            )}
          </VisibilitySensor>
        ))}

      {
        <VisibilitySensor offset={{ top: 20 }}>
          {({ isVisible }) => (
            <div
              className={
                isVisible
                  ? `${s.otherTechContainer} ${s.otherTechVisible}`
                  : `${s.otherTechContainer} ${a.hide}`
              }
            >
              <div className={s.otherTech}>{text[state.language].other}:</div>
              {other
                .sort((a, b) => a.iid - b.iid || a.name.localeCompare(b.name))
                .map(tech => (
                  <TechOther
                    key={`${tech.iid}${simpleName(tech.name)}`}
                    name={tech.name}
                    icon={
                      OtherIcons[simpleName(tech.name)]
                        ? OtherIcons[simpleName(tech.name)]
                        : unknown
                    }
                  />
                ))}
            </div>
          )}
        </VisibilitySensor>
      }
    </div>
  )
}

export default Abilities
