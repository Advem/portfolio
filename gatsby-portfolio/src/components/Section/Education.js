import React, { useContext } from 'react'
import { StateContext } from '../../context/ContextProvider'
import { text } from '../../context/langauges'
import s from '../../styles/education.module.scss'
import a from '../../styles/animations.module.scss'
import graduationSVG from '../../assets/graphics/graduation.svg'

const Education = props => {
  const state = useContext(StateContext)
  const data = text[state.language].education
  const alt = 'created by Advem Studio'
  return (
    <div
      className={
        props.isvisible
          ? `${s.container} ${a.education}`
          : `${s.container} ${a.hide}`
      }
    >
      <div className={s.textContainer}>
        <div className={s.textField}>
          <p className={s.textSmall}>{data[0]}</p>
          <p className={s.textMedium}>{data[1]}</p>
          <p className={s.textSmall}>{data[2]}</p>
        </div>

        <div className={s.textField}>
          <h2 className={s.textLarge}>{data[3]}</h2>
        </div>

        <div className={s.textField}>
          <div className={s.textSpec}>
            <h4 className={s.textLittle}>{data[4]}</h4>
            <h3 className={s.textNormal}>{data[5]}</h3>
          </div>
          <div className={s.textSpec}>
            <h4 className={s.textLittle}>{data[6]}</h4>
            <h3 className={s.textNormal}>{data[7]}</h3>
          </div>
        </div>
      </div>
      <div className={s.imageContainer}>
        <img src={graduationSVG} alt={alt} title={alt} />
      </div>
    </div>
  )
}

export default Education
