import React, { useContext } from 'react'
import mobileIcon from '../../assets/graphics/mobile.svg'
import notebookIcon from '../../assets/graphics/notebook.svg'
import s from '../../styles/intro.module.scss'
import a from '../../styles/animations.module.scss'
import { StateContext } from '../../context/ContextProvider'
import { text } from '../../context/langauges'

const Intro = props => {
  const state = useContext(StateContext)
  const data = text[state.language].intro
  const alt = 'created by Advem Studio'
  return (
    <div
      className={
        props.isvisible
          ? `${s.container} ${a.intro}`
          : `${s.container} ${a.hide}`
      }
    >
      <div className={s.contentContainer}>
        <img src={notebookIcon} alt={alt} title={alt} />
      </div>
      <div className={s.contentContainer}>
        <div className={s.textContainer}>
          <div>
            {data.designer[0]}
            <span>{data.designer[1]}</span>
          </div>
        </div>
        <div className={s.textContainer}>
          <div>
            {data.programmer[0]}
            <span>{data.programmer[1]}</span>
          </div>
          <span></span>
        </div>
        <div className={s.textLarge}>
          <div>
            <span>{data.inone[0]}</span>
          </div>
          <div>{data.inone[1]}</div>
        </div>
      </div>
      <div className={s.contentContainer}>
        <img src={mobileIcon} alt={alt} title={alt} />
      </div>
    </div>
  )
}

export default Intro
