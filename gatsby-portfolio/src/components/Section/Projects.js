import React, { useContext } from 'react'
import { StateContext } from '../../context/ContextProvider'
import { text } from '../../context/langauges'
import { useStaticQuery, graphql } from 'gatsby'
import s from '../../styles/projectSmall.module.scss'
import ProjectSmall from '../Elements/ProjectSmall'

import VisibilitySensor from 'react-visibility-sensor'

const Projects = props => {
  const state = useContext(StateContext)

  const contentfulData = useStaticQuery(graphql`
    query {
      allContentfulProject(sort: { fields: iid, order: ASC }) {
        edges {
          node {
            slug
            node_locale
            title
            subtitle
            type
            color
            phrase
            logo {
              title
              file {
                url
              }
              fixed(width: 60, toFormat: WEBP, quality: 100) {
                ...GatsbyContentfulFixed
              }
            }
            cover {
              title
              fluid(toFormat: WEBP, quality: 100) {
                ...GatsbyContentfulFluid
              }
            }
            shortDescription {
              shortDescription
            }
          }
        }
      }
    }
  `)

  return (
    <div className={s.container}>
      <h2 className={s.sectionHeadline}>{props.name}</h2>
      {contentfulData.allContentfulProject.edges
        .filter(edge => edge.node.node_locale === text[state.language].locale)
        .map((edge, id) => (
          <VisibilitySensor partialVisibility={false} key={id}>
            {({ isVisible }) => (
              <ProjectSmall data={edge.node} isvisible={isVisible ? 1 : 0} />
            )}
          </VisibilitySensor>
        ))}
    </div>
  )
}

export default Projects
