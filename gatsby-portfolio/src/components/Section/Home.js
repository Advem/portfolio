import React, { useContext, useState, useEffect } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import ImgBlur from '../Elements/ImgBlur'
import { StateContext } from '../../context/ContextProvider'
import { text } from '../../context/langauges'
import s from '../../styles/home.module.scss'
import background from '../../assets/graphics/homeBackground'

const Home = props => {
  const state = useContext(StateContext)
  const [printText, setPrintText] = useState('CODE')
  const [count, setCount] = useState(0)
  const delay = 1400

  useEffect(() => {
    const textLength = text[state.language].home.subtitle.length
    const interval = setInterval(() => {
      setPrintText(text[state.language].home.subtitle[count])
      count === textLength - 1 ? setCount(0) : setCount(count + 1)
    }, delay)
    return () => clearInterval(interval)
  })

  const image = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "assets/graphics/photoFace.png" }) {
        name
        childImageSharp {
          fluid {
            src
            base64
          }
        }
      }
    }
  `)

  return (
    <section className={s.container}>
      <div>{background}</div>
      <div className={s.textField}>
        <h2 className={s.title}>{text[state.language].home.title}</h2>
        <h3
          className={s.subtitle}
          key={count}
          css={{ animationDuration: `${delay / 1000}s` }}
        >
          {printText}
        </h3>
      </div>
      <div className={s.photosContainer}>
        <ImgBlur
          url={image.file.childImageSharp.fluid.src}
          base64={image.file.childImageSharp.fluid.base64}
          alt={image.file.name}
        />
      </div>
    </section>
  )
}

export default Home
