import React, { useContext } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { StateContext } from '../../context/ContextProvider'
import { themeBackground } from '../../context/theme'
import { text } from '../../context/langauges'

import s from '../../styles/contact.module.scss'
import a from '../../styles/animations.module.scss'

import graphs from '../../assets/graphics/graphs.svg'
import linkedin from '../../assets/icons/linkedin'
import mail from '../../assets/icons/mail'
import phone from '../../assets/icons/phone'
import gitlab from '../../assets/icons/gitlab'

const Contact = props => {
  const state = useContext(StateContext)

  const data = useStaticQuery(graphql`
    query {
      image: file(relativePath: { eq: "assets/graphics/avatarContact.png" }) {
        name
        childImageSharp {
          fixed(width: 60, quality: 100, jpegProgressive: true) {
            src
          }
        }
      }
      english: contentfulMedia(node_locale: { eq: "en-US" }) {
        cv {
          file {
            url
          }
        }
      }
      polish: contentfulMedia(node_locale: { eq: "pl" }) {
        cv {
          file {
            url
          }
        }
      }
    }
  `)

  const backgroundStyle = {
    background: `rgba(${themeBackground(state.theme)}, 0.9)`,
    '& a div': {
      opacity: '0.6',
      '&:hover': {
        opacity: '1',
        backgroundColor: `rgba(${themeBackground(state.theme, true)}, 1)`,
        color: `rgba(${themeBackground(state.theme)}, 1)`,
      },
    },
  }

  const buttonStyle = {
    backgroundColor: `rgba(${themeBackground(state.theme, true)}, 1)`,
    color: `rgba(${themeBackground(state.theme)}, 1)`,
    '&:hover': {
      backgroundColor: `rgba(${themeBackground(state.theme)}, 1)`,
      color: `rgba(${themeBackground(state.theme, true)}, 1)`,
    },
  }

  const textMain =
    state.language === 'english' ? (
      <>
        <h4>
          <span>Let's</span> make these graphs <span>grow</span>...
        </h4>
        <p>...just start with a simple mail</p>
      </>
    ) : (
      <>
        <h4>
          Poszerz<span>my</span> wspólnie horyzon<span>ty</span>...
        </h4>
        <p>...zacznij od wysłania prostej wiadomości</p>
      </>
    )

  const contact = [
    'Adam Drabik',
    'Advem Studio',
    'aadamdrabik@gmail.com',
    '+48 533 164 479',
    'https://gitlab.com/Advem',
    'https://www.linkedin.com/in/adam-drabik/',
  ]

  return (
    <div
      className={
        props.isvisible
          ? `${s.container} ${a.contact}`
          : `${s.container} ${a.hide}`
      }
    >
      <img className={s.backgroundImage} src={graphs} alt="graphs" />
      <h2 className={s.sectionHeadline}>{props.name}</h2>
      <div className={s.text}>{textMain}</div>
      <div className={s.links} css={backgroundStyle}>
        <div className={s.info}>
          {data.image && (
            <img src={data.image.childImageSharp.fixed.src} alt={contact[0]} />
          )}
          <div>
            <p>{contact[0]}</p>
            <p>{contact[1]}</p>
          </div>
        </div>
        <a href={`mailto:${contact[2]}`}>
          <div className={s.link}>
            {mail}
            {contact[2]}
          </div>
        </a>
        <a href={`tel:${contact[3]}`}>
          <div className={s.link}>
            {phone}
            {contact[3]}
          </div>
        </a>
        <a href={contact[4]} target="_blank" rel="noopener noreferrer">
          <div className={s.link}>
            {gitlab}
            {text[state.language].gitlab}
          </div>
        </a>
        <a href={contact[5]} target="_blank" rel="noopener noreferrer">
          <div className={s.link}>
            {linkedin}
            {text[state.language].linkedin}
          </div>
        </a>
      </div>
      <a
        css={buttonStyle}
        className={props.isvisible ? `${s.download} ${a.download}` : s.download}
        href={data[state.language].cv.file.url}
        target="_blank"
        rel="noopener noreferrer"
      >
        <div>{`${text[state.language].download} CV (${
          text[state.language].languageShortcut
        })`}</div>
      </a>
    </div>
  )
}

export default Contact
