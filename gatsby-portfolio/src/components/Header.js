import React, { useContext, useState, useEffect } from 'react'
import { StateContext } from '../context/ContextProvider'

import { text } from '../context/langauges'
// import { css } from 'glamor'

import Logo from '../components/Elements/Logo'
import MenuButton from '../components/Elements/MenuButton'
import BurgerButton from '../components/Elements/BurgerButton'
import MenuMobile from '../components/Elements/MenuMobile'

import s from '../styles/header.module.scss'

import adjustIcon from '../assets/icons/adjust'
import burgerIcon from '../assets/icons/burger'
import polishIcon from '../assets/icons/polish'
import englishIcon from '../assets/icons/english'
// import { themeBackground } from '../context/theme'

const Header = () => {
  const [scrolled, setScrolled] = useState(false)
  const state = useContext(StateContext)
  // const backgroundStyle = css({
  //   backgroundColor: `rgba(${themeBackground(state.theme)}, 0.9)`,
  // })

  useEffect(() => {
    function handleScroll() {
      const isTop = window.scrollY > 100
      if (isTop !== scrolled) {
        setScrolled(isTop)
      }
    }
    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  })

  return (
    <>
      <header
        className={`${s.header}  ${scrolled &&
          !state.menuOn &&
          s.blurBackground}`} //${scrolled && backgroundStyle}
      >
        <Logo />
        <div className={s.container} role="menu">
          <BurgerButton
            icon={burgerIcon}
            data={text[state.language].menu}
            name="menu"
          >
            <MenuMobile />
          </BurgerButton>

          <MenuButton
            icon={burgerIcon}
            data={text[state.language].menu}
            name="menu"
          />
          <MenuButton
            icon={adjustIcon}
            data={[text[state.language][state.theme]]}
            name="theme"
          />
          <MenuButton
            icon={state.language === 'english' ? englishIcon : polishIcon}
            data={[text[state.language][state.language]]}
            name="language"
          />
        </div>
      </header>
    </>
  )
}

export default Header
