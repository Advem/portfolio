import React from 'react'
import Section from './Section'
import VisibilitySensor from 'react-visibility-sensor'
// Sections
import Home from '../components/Section/Home'
import Intro from '../components/Section/Intro'
import About from '../components/Section/About'
import Education from '../components/Section/Education'
import Abilities from '../components/Section/Abilities'
import Projects from '../components/Section/Projects'
import Contact from '../components/Section/Contact'

const Components = {
  Home,
  Intro,
  About,
  Education,
  Abilities,
  Projects,
  Contact,
}

export default section => {
  if (typeof Components[section.component] !== 'undefined') {
    return (
      <VisibilitySensor
        offset={{ bottom: 300 }}
        partialVisibility={true}
        key={section.component}
      >
        {({ isVisible }) => (
          <Section id={section.component} name={section.name}>
            {React.createElement(Components[section.component], {
              name: section.name,
              component: section.component,
              isvisible: isVisible,
            })}
          </Section>
        )}
      </VisibilitySensor>
    )
  }
  return React.createElement(() => (
    <div>
      Component: {section.component}({section.name}) has not been created!
    </div>
  ))
}
