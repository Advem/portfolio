import React, { useContext } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { StateContext } from '../context/ContextProvider'
import { text } from '../context/langauges'
import Layout from '../components/Layout'
import Head from '../components/Head'
import Components from '../components/components'

import '../styles/index.scss'

const IndexPage = () => {
  const state = useContext(StateContext)

  const renderOuterSections = () =>
    text[state.language].outer.map(section => Components(section))

  const renderMenuSections = () =>
    text[state.language].menu.map(section => Components(section))

  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          author
          description
          url
        }
      }
    }
  `)

  const meta = {
    title: `${data.site.siteMetadata.author} Portfolio`,
  }

  return (
    <Layout>
      <Head {...meta} />
      {renderOuterSections()}
      {renderMenuSections()}
    </Layout>
  )
}

export default IndexPage
