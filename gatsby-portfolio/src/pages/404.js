import React from 'react'
import { Link } from 'gatsby'
import Head from '../components/Head'
import Layout from '../components/Layout'
import arrowLeft from '../assets/icons/arrowLeft'

const NotFound = () => {
  const styling = {
    width: '100vw',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    '& h1': {
      fontSize: '40px',
      fontWeight: '300',
      opacity: '0.4',
    },
    '& p': {
      padding: '20px',
      fontSize: '60px',
      fontWeight: '600',
      color: 'hsl(264, 50%, 50%)',
      opacity: 0.67,
      textAlign: 'center',
      '&:hover': {
        opacity: 1,
      },
      '& svg': {
        width: '36px',
        paddingRight: '20px',
      },
    },
  }

  const meta = {
    title: `NOT FOUND`,
  }

  return (
    <Layout>
      <Head {...meta} />
      <div css={styling}>
        <h1>Page not found?!</h1>
        <p>
          {arrowLeft}
          <Link to="/">Back</Link>
        </p>
      </div>
    </Layout>
  )
}

export default NotFound
