import React from 'react'
import Projects from '../components/Section/Projects'

import Layout from '../components/Layout'
import Head from '../components/Head'

const Project = () => {
  const meta = {
    title: `Projects`,
  }

  return (
    <Layout>
      <Head {...meta} />
      <Projects />
    </Layout>
  )
}

export default Project
