export const timeToToday = date => {
  let initial = new Date(date)
  let difference = new Date(Date.now() - initial)
  return Math.abs(difference.getUTCFullYear() - 1970)
}

export const text = {
  english: {
    languageShortcut: 'EN',
    menu: [
      { name: 'About', component: 'About' },
      { name: 'Education', component: 'Education' },
      { name: 'Abilities', component: 'Abilities' },
      { name: 'Projects', component: 'Projects' },
      { name: 'Contact', component: 'Contact' },
    ],
    outer: [
      { name: 'Home', component: 'Home' },
      { name: 'Intro', component: 'Intro' },
    ],
    home: {
      title: 'Desire to',
      subtitle: ['CREATE', 'COMPOSE', 'DESIGN', 'DEVELOP', 'EVOLVE'],
    },
    intro: {
      designer: ['Design', 'er'],
      programmer: ['Program', 'mer'],
      inone: ['IN', 'ONE'],
    },
    education: [
      'Graduated from the',
      'Gdańsk University of Technology',
      'in February 2020 with a degree of',
      'Software Engineer',
      'Field',
      'Technical Physics',
      'Specialization',
      'Applied Informatics',
    ],
    other: 'Other',
    explore: 'Explore',
    technologies: 'Technologies',
    description: 'Description',
    published: 'Published',
    projects: 'Projects',
    repo: 'Repo',
    docs: 'Docs',
    visit: 'Visit',
    dark: 'Dark',
    light: 'Light',
    english: 'English',
    polish: 'Polish',
    locale: 'en-US',
    level: ['Beginner', 'Junior', 'Regular', 'Advanced', 'Master'],
    linkedin: 'LinkedIn Profile',
    gitlab: 'GitLab Repository',
    download: 'Download',
    hello: 'Hello',
    hellosub: 'A little bit about me first',
    about: [
      {
        title: null,
        text: `Adam is ${timeToToday(
          '1997-02-01'
        )} years old who lives in Gdańsk, Poland. For the past 4 years he has been studying at the Gdańsk University of Technology (Physics and Applied Informatics) and has graduated with a degree of Software Engineer several months ago. During studies he learned, apart from physics, about computer science, technologies and programming, but particularly got interested in web development which currently is his main advancement path.`,
      },
      {
        title: 'Motivation',
        text:
          '"Once I was convinced that video game development is what I would like to do in the future. I have always loved drawing, later creating computer graphics. Combining the practical with the pleasurable, I found out that game programming is the key to success so I started learning to program. In college times, I learned basic web technologies: HTML, CSS, later PHP, SQL, and finally JavaScript. I noticed then that not only game development, but also Front-end is the desired combination of graphics and code. During my first job in IT as a Front-end Intern, I got to know the React library. This tool interested me so much that I decided to write an engineering project using this technology. Currently, I develop my web app based on React and learn other web technologies"',
      },
      {
        title: 'Hobby',
        text:
          "In his spare time he creates digital art or produces music as a hobby. Used to skiing and play handball. He also loves sci-fi movies, especially those which don't break laws of physics that much...",
      },
    ],
  },
  polish: {
    languageShortcut: 'PL',
    menu: [
      { name: 'O mnie', component: 'About' },
      { name: 'Edukacja', component: 'Education' },
      { name: 'Umiejętności', component: 'Abilities' },
      { name: 'Projekty', component: 'Projects' },
      { name: 'Kontakt', component: 'Contact' },
    ],
    outer: [
      { name: 'Home', component: 'Home' },
      { name: 'Intro', component: 'Intro' },
    ],
    home: {
      title: 'Pragnienie',
      subtitle: ['KREACJI', 'STYLU', 'TWORZENIA', 'KOMPOZYCJI', 'ROZWOJU'],
    },
    intro: {
      designer: ['Projekt', 'ant'],
      programmer: ['Program', 'ista'],
      inone: ['W', 'JEDNYM'],
    },
    education: [
      'Ukończona uczelnia wyższa',
      'Politechnika Gdańska',
      'w lutym 2020 z tytułem',
      'Inżynier Oprogramowania',
      'Kierunek',
      'Fizyka Techniczna',
      'Specjalizacja',
      'Informatyka Stosowana',
    ],
    other: 'Inne',
    explore: 'Zobacz',
    technologies: 'Technologie',
    published: 'Opublikowano',
    description: 'Opis',
    projects: 'Projekty',
    visit: 'Odwiedź',
    repo: 'Repo',
    docs: 'Dokumentacja',
    dark: 'Ciemny',
    light: 'Jasny',
    english: 'Angielski',
    polish: 'Polski',
    locale: 'pl',
    level: ['Początkujący', 'Junior', 'Regularny', 'Zaawansowany', 'Ekspert'],
    linkedin: 'Profil LinkedIn',
    gitlab: 'Repozytorium GitLab',
    download: 'Pobierz',
    hello: 'Dzień dobry',
    hellosub: 'Na wstępie trochę o mnie',
    about: [
      {
        title: null,
        text: `Adam to ${timeToToday(
          '1997-02-01'
        )} letni mieszkaniec Gdańska. Przez ostatnie 4 lata studiował na Politechnice Gdańskiej (Fizyka i Informatyka Stosowana), a kilka miesięcy temu uzyskał tytuł Inżyniera Oprogramowania. Podczas nauki na uczelni, po za fizyką, zdobył wiedzę z zakresu nauk komputerowych, technologii oraz programowania, ale szczególnie zainteresowało go tworzenie aplikacji internetowych, co aktualnie jest jego główną ścieżką rozwoju`,
      },
      {
        title: 'Motywacja',
        text:
          '"Kiedyś byłem przekonany, że produkcja gier komputerowych jest tym, co chciałbym robić w przyszłości. Od zawsze lubiłem rysować, poźniej tworzyć grafikę komputerową. Chcąc połączyć przyjemne z pożytecznym, uznałem, że tworzenie gier jest kluczem do sukcesu i zacząłem uczyć się programowania. Będąc na studiach, poznałem podstawowe technologie webowe: HTML, CSS, poźniej PHP, SQL i wreszcie JavaScript. Zauważyłem wówczas, że nie tylko game development, ale także Front-end jest porządanym przez mnie połączeniem grafiki i kodu. Podczas pierwszej pracy w IT jako stażysta Front-end poznałem bibliotekę React. Narzędzie to na tyle mnie zainteresowało, że postanowiłem napisać projekt inżynierski, wykorzystując tę technologię. Aktualnie zajmuje się tworzeniem własnej aplikacji, opierającą się na React i nauką kolejnych technologii webowych"',
      },
      {
        title: 'Hobby',
        text:
          'W wolnym czasie zajmuje się sztuką cyfrową lub produkcją muzyki. Niegdyś uprawiał narciarstwo alpejskie oraz grał w piłkę ręczną. Uwielbia również filmy Sci-fi szczególnie te, które w sposób oczywisty nie łamią praw fizyki...',
      },
    ],
  },
}
