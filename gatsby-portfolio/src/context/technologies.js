export const technologies = [
  { id: 1, name: 'React', level: 52 },
  { id: 2, name: 'Redux', level: 48 },
  { id: 3, name: 'GraphQL', level: 20 },
  { id: 4, name: 'NodeJS', level: 30 },
  { id: 5, name: 'JavaScript', level: 56 },
  { id: 6, name: 'HTML5', level: 50 },
  { id: 7, name: 'CSS', level: 66 },
  { id: 8, name: 'Sass', level: 61 },
  { id: 9, name: 'SocketIO', level: 56 },
  { id: 10, name: 'Gatsby', level: 38 },
  { id: 11, name: 'Adobe XD', level: 65 },
  { id: 12, name: 'Adobe Photoshop', level: 70 },
  { id: 13, name: 'Adobe Illustrator', level: 50 },
  { id: 14, name: 'English C1', level: 80 },
  { id: 15, name: 'Polish Native', level: 95 },
  { id: 4, name: 'Jest', level: 18 },
]

export const other = [
  { id: 1, name: 'webpack' },
  { id: 2, name: 'Git' },
  { id: 3, name: 'C++' },
  { id: 4, name: 'PHP' },
  { id: 5, name: 'npm' },
  { id: 6, name: 'Laravel' },
  { id: 7, name: 'MySQL' },
  { id: 8, name: 'AWS' },
]
