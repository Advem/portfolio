export const theme = (theme, contrast = false) =>
  theme === 'dark'
    ? contrast
      ? '0,0,0'
      : '255,255,255'
    : contrast
    ? '255,255,255'
    : '0,0,0'

export const themeBackground = (theme, contrast = false) =>
  theme === 'dark'
    ? contrast
      ? '255,255,255'
      : '23,23,23'
    : contrast
    ? '23,23,23'
    : '255,255,255'
