import React, { useReducer } from 'react'

export const StateContext = React.createContext()
export const DispatchContext = React.createContext()

const initailState = {
  theme: 'light',
  language: 'english',
  menuOn: false,
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'TOGGLE_THEME':
      return {
        ...state,
        theme: state.theme === 'dark' ? 'light' : 'dark',
      }
    case 'TOGGLE_LANGUAGE':
      return {
        ...state,
        language: state.language === 'english' ? 'polish' : 'english',
      }
    case 'TOGGLE_MENU':
      return { ...state, menuOn: !state.menuOn }
    case 'SET_THEME':
      return {
        ...state,
        theme: action.payload,
      }
    default:
      throw new Error('Incorrect Action')
  }
}

const ContextProvider = props => {
  const [state, dispatch] = useReducer(reducer, initailState)
  return (
    <StateContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {props.children}
      </DispatchContext.Provider>
    </StateContext.Provider>
  )
}

export default ContextProvider
