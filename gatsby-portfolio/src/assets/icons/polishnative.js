import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="76.315"
    height="50"
    viewBox="0 0 76.315 50"
  >
    <g transform="translate(0 -88.276)">
      <path
        d="M0,275.284A5.715,5.715,0,0,0,5.715,281H70.6a5.715,5.715,0,0,0,5.715-5.715V256H0Z"
        transform="translate(0 -142.724)"
        fill="#ff4b55"
      />
      <path
        d="M70.6,88.276H5.715A5.715,5.715,0,0,0,0,93.991v19.284H76.315V93.991A5.715,5.715,0,0,0,70.6,88.276Z"
        transform="translate(0 0)"
        fill="#f5f5f5"
      />
    </g>
  </svg>
)
