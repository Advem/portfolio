import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="56.944"
    height="64"
    viewBox="0 0 56.944 64"
  >
    <g transform="translate(0 -0.268)">
      <path
        d="M56.941,19.084a5.651,5.651,0,0,0-.7-2.843,5.437,5.437,0,0,0-2.06-2.007Q42.813,7.677,31.437,1.129a5.584,5.584,0,0,0-6.052.06C22.368,2.968,7.261,11.624,2.759,14.231A5.258,5.258,0,0,0,0,19.082Q0,32.268,0,45.453a5.676,5.676,0,0,0,.666,2.789A5.418,5.418,0,0,0,2.757,50.3c4.5,2.607,19.611,11.262,22.628,13.042a5.585,5.585,0,0,0,6.053.06q11.368-6.564,22.749-13.1a5.42,5.42,0,0,0,2.091-2.061,5.684,5.684,0,0,0,.666-2.789s0-17.576,0-26.367"
        transform="translate(0 0)"
        fill="#5c8dbc"
      />
      <path
        d="M30.881,143.509,2.988,159.584a5.42,5.42,0,0,0,2.091,2.064c4.5,2.61,19.61,11.272,22.627,13.053a5.58,5.58,0,0,0,6.053.06q11.368-6.569,22.749-13.116a5.422,5.422,0,0,0,2.091-2.063L30.881,143.509"
        transform="translate(-2.322 -111.356)"
        fill="#1a4674"
      />
      <path
        d="M91.1,148.27a9.422,9.422,0,0,0,16.394-.055l-8.125-4.707L91.1,148.27"
        transform="translate(-70.811 -111.332)"
        fill="#1a4674"
      />
      <path
        d="M156.589,74.8a5.638,5.638,0,0,0-.7-2.841l-27.71,15.927,27.744,16.044a5.67,5.67,0,0,0,.666-2.787s0-17.561,0-26.344"
        transform="translate(-99.648 -55.704)"
        fill="#1b598e"
      />
      <path
        d="M198.5,133.554h-2.168v2.168h-2.168v-2.168H192v-2.168h2.168v-2.168h2.168v2.168H198.5v2.168m-7.911,0h-2.168v2.168h-2.168v-2.168h-2.168v-2.168h2.168v-2.168h2.168v2.168h2.168v2.168"
        transform="translate(-143.087 -100.223)"
        fill="#fff"
      />
      <path
        d="M70.055,82.532a9.411,9.411,0,1,1-.061-9.336l8.238-4.737a18.9,18.9,0,1,0,.049,18.831l-8.225-4.759"
        transform="translate(-33.368 -45.65)"
        fill="#fff"
      />
    </g>
  </svg>
)
