import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="60.062"
    height="60"
    viewBox="0 0 60.062 60"
  >
    <g transform="translate(-3.333 -3.449)">
      <path
        d="M25.758,4.4A30.028,30.028,0,0,1,63.381,32.772,30.014,30.014,0,1,1,25.758,4.4Z"
        transform="translate(0 0)"
        fill="#010101"
      />
      <path
        d="M38.364,22.7C53.039,16.819,71.039,27.668,72.558,43.4c2.365,14.159-9.387,28.548-23.732,28.963C35.182,73.729,22,62.163,21.611,48.463,20.593,37.371,27.9,26.364,38.364,22.7Z"
        transform="translate(-13.792 -13.369)"
        fill="#fff"
      />
      <path
        d="M81.516,70.72c6.6-5.366,13.056-10.918,19.8-16.127C97.779,60,94.141,65.339,90.6,70.748c-3.034.014-6.068.014-9.088-.029Zm5.579,2.99c3.048,0,6.082,0,9.116.043C89.569,79.091,83.141,84.686,76.383,89.88,79.92,84.471,83.558,79.12,87.1,73.711Z"
        transform="translate(-55.493 -38.791)"
        fill="#010101"
      />
    </g>
  </svg>
)
