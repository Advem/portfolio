import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="53.399"
    height="60"
    viewBox="0 0 53.399 60"
  >
    <g transform="translate(-25.74 -4.2)">
      <g transform="translate(28.485 6.222)">
        <g transform="translate(0 0)">
          <rect
            width="2.626"
            height="50.668"
            transform="matrix(-0.866, -0.5, 0.5, -0.866, 2.274, 45.192)"
            fill="#e535ab"
          />
        </g>
      </g>
      <g transform="translate(27.964 45.203)">
        <rect width="48.935" height="2.626" fill="#e535ab" />
      </g>
      <g transform="translate(28.967 43.849)">
        <g transform="translate(0 0)">
          <rect
            width="29.265"
            height="2.626"
            transform="matrix(-0.866, -0.5, 0.5, -0.866, 25.344, 16.907)"
            fill="#e535ab"
          />
        </g>
      </g>
      <g transform="translate(49.25 7.66)">
        <g transform="translate(0 0)">
          <rect
            width="29.265"
            height="2.626"
            transform="matrix(-0.866, -0.5, 0.5, -0.866, 25.344, 16.907)"
            fill="#e535ab"
          />
        </g>
      </g>
      <g transform="translate(28.974 7.65)">
        <g transform="translate(0 0)">
          <rect
            width="2.626"
            height="29.265"
            transform="matrix(-0.5, -0.866, 0.866, -0.5, 1.313, 16.907)"
            fill="#e535ab"
          />
        </g>
      </g>
      <g transform="translate(48.792 6.222)">
        <g transform="translate(0 0)">
          <rect
            width="50.668"
            height="2.626"
            transform="matrix(-0.5, -0.866, 0.866, -0.5, 25.334, 45.192)"
            fill="#e535ab"
          />
        </g>
      </g>
      <g transform="translate(29.973 19.567)">
        <g transform="translate(0)">
          <rect width="2.626" height="29.265" fill="#e535ab" />
        </g>
      </g>
      <g transform="translate(72.28 19.567)">
        <rect width="2.626" height="29.265" fill="#e535ab" />
      </g>
      <g transform="translate(50.983 44.931)">
        <g transform="translate(0 0)">
          <rect
            width="2.294"
            height="25.453"
            transform="matrix(-0.5, -0.866, 0.866, -0.5, 1.147, 14.713)"
            fill="#e535ab"
          />
        </g>
      </g>
      <path
        d="M314.648,253.823a5.524,5.524,0,1,1-2.025-7.546,5.529,5.529,0,0,1,2.025,7.546"
        transform="translate(-236.261 -204.623)"
        fill="#e535ab"
      />
      <path
        d="M36.048,92.923a5.524,5.524,0,1,1-2.025-7.546,5.529,5.529,0,0,1,2.025,7.546"
        transform="translate(0 -68.202)"
        fill="#e535ab"
      />
      <path
        d="M26.577,253.823a5.524,5.524,0,1,1,7.546,2.025,5.533,5.533,0,0,1-7.546-2.025"
        transform="translate(-0.084 -204.623)"
        fill="#e535ab"
      />
      <path
        d="M305.177,92.923a5.524,5.524,0,1,1,7.546,2.025,5.533,5.533,0,0,1-7.546-2.025"
        transform="translate(-236.345 -68.202)"
        fill="#e535ab"
      />
      <path
        d="M170.621,337.042a5.521,5.521,0,1,1,5.521-5.521,5.516,5.516,0,0,1-5.521,5.521"
        transform="translate(-118.181 -272.842)"
        fill="#e535ab"
      />
      <path
        d="M170.621,15.242a5.521,5.521,0,1,1,5.521-5.521,5.516,5.516,0,0,1-5.521,5.521"
        transform="translate(-118.181 0)"
        fill="#e535ab"
      />
    </g>
  </svg>
)
