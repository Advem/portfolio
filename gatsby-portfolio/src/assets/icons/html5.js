import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="52.906"
    height="60"
    viewBox="0 0 52.906 60"
  >
    <g transform="translate(0)">
      <path
        d="M56.006,0,51.187,53.993,29.521,60,7.915,54,3.1,0Z"
        transform="translate(-3.1)"
        fill="#e44d26"
      />
      <path
        d="M886.4,198.32l17.652-4.847L908.2,147.4H886.4Z"
        transform="translate(-860.169 -142.949)"
        fill="#f16529"
      />
      <path
        d="M339.1,375.191h9.448V368.6H331.8l.16,1.767,1.643,18.161h14.944V381.94h-8.835Zm1.507,16.632h-6.7l.936,10.348,13.682,3.748.03-.009v-6.856l-.03.009-7.437-1.981-.477-5.259Z"
        transform="translate(-321.994 -357.47)"
        fill="#ebebeb"
      />
      <path
        d="M885.6,388.625h8.222l-.776,8.546-7.449,1.984v6.859l13.691-3.745.1-1.114,1.57-17.35.163-1.77H885.6v6.591Zm0-13.352v.015h16.13l.133-1.481.3-3.34.16-1.767H885.6v6.573Z"
        transform="translate(-859.273 -357.567)"
        fill="#fff"
      />
    </g>
  </svg>
)
