import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="52.908"
    height="60"
    viewBox="0 0 52.908 60"
  >
    <g transform="translate(0)">
      <path d="M4.816,54,0,0H52.908L48.086,54,26.421,60Z" fill="#1572b6" />
      <path
        d="M885.5,198.774l17.548-4.853,4.127-46.122H885.5Z"
        transform="translate(-859.109 -143.374)"
        fill="#33a9dc"
      />
      <path
        d="M885.5,367.534h8.786l.6-6.8H885.5V354.1h16.65l-.159,1.782-1.632,18.3H885.5Z"
        transform="translate(-859.075 -343.534)"
        fill="#fff"
      />
      <path
        d="M415.157,1142.292h-.03l-7.394-2L407.26,1135H400.6l.931,10.424,13.6,3.785h.039v-6.918Z"
        transform="translate(-388.643 -1101.117)"
        fill="#ebebeb"
      />
      <path
        d="M894.305,1014.8l-.8,8.885-7.406,2v6.918l13.61-3.773.1-1.123,1.156-12.907h-6.66Z"
        transform="translate(-859.653 -984.514)"
        fill="#fff"
      />
      <path
        d="M347.532,354.1v6.639H331.5l-.138-1.491-.3-3.366L330.9,354.1Zm-.024,13.434v6.639H340.2l-.129-1.491-.3-3.366-.159-1.782Z"
        transform="translate(-321.025 -343.534)"
        fill="#ebebeb"
      />
    </g>
  </svg>
)
