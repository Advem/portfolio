import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="102.857"
    height="40"
    viewBox="0 0 102.857 40"
  >
    <path
      d="M0,0H102.857V34.286H51.429V40H28.571V34.286H0ZM5.714,28.571H17.143V11.429h5.714V28.571h5.714V5.714H5.714ZM34.286,5.714V34.286H45.714V28.571H57.143V5.714Zm11.429,5.714h5.714V22.857H45.714ZM62.857,5.714V28.571H74.286V11.429H80V28.571h5.714V11.429h5.714V28.571h5.714V5.714Z"
      fill="#cb3837"
    />
    <path
      d="M1,23.922H12.461V6.731h5.731V23.922h5.731V1H1Z"
      transform="translate(4.731 4.731)"
      fill="#fff"
    />
    <path
      d="M6,1V29.539H17.461V23.831H28.922V1ZM23.192,18.123H17.461V6.708h5.731Z"
      transform="translate(28.258 4.731)"
      fill="#fff"
    />
    <path
      d="M11,1V23.922H22.461V6.731h5.731V23.922h5.731V6.731h5.731V23.922h5.731V1Z"
      transform="translate(51.743 4.731)"
      fill="#fff"
    />
  </svg>
)
