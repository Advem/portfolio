import React from 'react'
import s from '../../styles/menu.module.scss'

export default (
  <>
    <div className={`${s.burgerTop}`}></div>
    <div className={`${s.burgerMid}`}></div>
    <div className={`${s.burgerBot}`}></div>
  </>
)
