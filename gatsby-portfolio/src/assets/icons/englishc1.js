import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="76.315"
    height="50"
    viewBox="0 0 76.315 50"
  >
    <g transform="translate(0 -88.275)">
      <path
        d="M70.6,88.276H5.715A5.715,5.715,0,0,0,0,93.991V132.56a5.715,5.715,0,0,0,5.715,5.715H70.6a5.715,5.715,0,0,0,5.715-5.715V93.991A5.715,5.715,0,0,0,70.6,88.276Z"
        transform="translate(0 -0.001)"
        fill="#41479b"
      />
      <path
        d="M76.236,93.046A5.716,5.716,0,0,0,70.6,88.275H69.113L44.737,104.246V88.275H31.579v15.971L7.2,88.275H5.715A5.716,5.716,0,0,0,.079,93.046L20.914,106.7H0v13.158H20.914L.079,133.5a5.716,5.716,0,0,0,5.636,4.771H7.2L31.579,122.3v15.971H44.737V122.3l24.376,15.971H70.6a5.716,5.716,0,0,0,5.636-4.771L55.4,119.854H76.315V106.7H55.4Z"
        transform="translate(0 0)"
        fill="#f5f5f5"
      />
      <g transform="translate(0 88.275)">
        <path
          d="M42.105,88.276H34.21v21.052H0v7.895H34.21v21.052h7.895V117.223h34.21v-7.895H42.105Z"
          transform="translate(0 -88.276)"
          fill="#ff4b55"
        />
        <path
          d="M11.541,318.164l27.77-18.026H34.485l-25.261,16.4A5.715,5.715,0,0,0,11.541,318.164Z"
          transform="translate(-7.851 -268.532)"
          fill="#ff4b55"
        />
        <path
          d="M318.786,300.138H313.96l26.9,17.459a5.733,5.733,0,0,0,1.829-1.946Z"
          transform="translate(-267.12 -268.532)"
          fill="#ff4b55"
        />
        <path
          d="M4.049,97.871,27.525,113.11h4.826L5.75,95.842A5.73,5.73,0,0,0,4.049,97.871Z"
          transform="translate(-3.446 -94.716)"
          fill="#ff4b55"
        />
        <path
          d="M304.964,108.671l25.307-16.428a5.715,5.715,0,0,0-2.337-1.616l-27.8,18.044Z"
          transform="translate(-255.355 -90.277)"
          fill="#ff4b55"
        />
      </g>
    </g>
  </svg>
)
