import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xlink="http://www.w3.org/1999/xlink"
    viewBox="0 0 508.527 503.213"
  >
    <title>SVG by Advem Studio</title>
    <defs>
      <linearGradient
        id="a"
        x1="1"
        y1="0.5"
        x2="0.001"
        y2="0.5"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#331a54" />
        <stop offset="1" stopColor="#3d3d91" />
      </linearGradient>
      <linearGradient
        id="b"
        y1="0.5"
        x2="1"
        y2="0.5"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#8b8bdb" />
        <stop offset="1" stopColor="#639" />
      </linearGradient>
      <linearGradient
        id="c"
        x1="0.39"
        y1="1"
        x2="0.736"
        y2="0.253"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#ff7300" />
        <stop offset="0.26" stopColor="#ed9002" />
        <stop offset="0.43" stopColor="#e59e03" />
        <stop offset="0.62" stopColor="#e5b601" />
        <stop offset="0.77" stopColor="#e5c500" />
        <stop offset="0.923" stopColor="#bf6f00" />
        <stop offset="1" stopColor="#bf6f00" />
      </linearGradient>
      <linearGradient
        id="d"
        x1="0.377"
        y1="0.982"
        x2="0.608"
        y2="0.018"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#4500bf" />
        <stop offset="0.47" stopColor="#8c00ff" />
        <stop offset="1" stopColor="#96b2ff" />
      </linearGradient>
      <linearGradient
        id="e"
        x1="0.438"
        y1="0.963"
        x2="0.552"
        y2="0.035"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#ff7300" />
        <stop offset="0.18" stopColor="#f97c01" />
        <stop offset="0.47" stopColor="#ea9602" />
        <stop offset="0.55" stopColor="#e59e03" />
        <stop offset="0.87" stopColor="#e5c500" />
        <stop offset="0.99" stopColor="#bf6f00" />
      </linearGradient>
      <linearGradient
        id="f"
        x1="0.601"
        y1="0.643"
        x2="0.235"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#fff" stopColor="0" />
        <stop offset="1" stopColor="#fff" />
      </linearGradient>
      <linearGradient
        id="g"
        x1="0.5"
        y1="1"
        x2="0.5"
        y2="-2.454"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#b54100" />
        <stop offset="0.06" stopColor="#b14b01" />
        <stop offset="0.23" stopColor="#a96002" />
        <stop offset="0.39" stopColor="#a56d03" />
        <stop offset="0.54" stopColor="#a37103" />
        <stop offset="0.87" stopColor="#e5c500" />
        <stop offset="0.976" stopColor="#bf6f00" />
        <stop offset="1" stopColor="#bf6f00" />
      </linearGradient>
      <linearGradient
        id="h"
        x1="-0.053"
        y1="0.109"
        x2="1.203"
        y2="0.979"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#fff" />
        <stop offset="0.01" stopColor="#ff9400" />
        <stop offset="1" stopColor="#ff9400" stopColor="0" />
      </linearGradient>
      <linearGradient
        id="i"
        x1="-0.053"
        y1="0.109"
        x2="1.203"
        y2="0.979"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#ff9400" />
        <stop offset="1" stopColor="#ff9400" stopColor="0" />
      </linearGradient>
    </defs>
    <g transform="translate(0 -26)">
      <path
        d="M508.527,431.167c0,79.163-115.338,192.1-256.134,192.1S0,510.29,0,431.127,109.757,283.07,250.519,283.07,508.527,352,508.527,431.167Z"
        transform="translate(0 -138.256)"
        fill="url(#a)"
      />
      <path
        d="M254.264,0C394.69,0,508.527,65.03,508.527,145.248S392.313,285.136,251.887,285.136C105.71,288.214.289,230.587,0,145.248,0,65.03,113.838,0,254.264,0Z"
        transform="translate(0 144.814)"
        fill="url(#b)"
      />
      <path
        d="M227.847,549.994,89.89,538.859V501.94L270.972,365.47l85.965,85.164-106.1,93.6Z"
        transform="translate(-28.862 -114.673)"
        fill="url(#c)"
      />
      <path
        d="M313.036,298.393,397.7,351.607c14.427,9.07,32.731-.984,34.387-18.888l21.474-231.356c1-10.795-4.623-20.979-13.857-25.222L353.14,36.656c-14.257-6.5-30.28,3.768-31.78,20.368l-19.6,217.635C300.9,284.265,305.283,293.566,313.036,298.393Z"
        transform="translate(-96.854 -8.463)"
        fill="#321066"
      />
      <path
        d="M323.956,286.565l87.88,58.116c6.552,4.331,14.862-.475,15.615-9.036l20.055-227.35c.448-5.153-2.1-10.041-6.307-12.058l-89.76-43.084c-6.477-3.116-13.755,1.8-14.434,9.736L318.837,275.214A12.309,12.309,0,0,0,323.956,286.565Z"
        transform="translate(-102.356 -14.09)"
        fill="url(#d)"
      />
      <path
        d="M323.961,362.077l87.88,58.116c6.552,4.332,14.862-.475,15.615-9.036l6.11-69.25c-4.6,7.685-9.5-5.391-18.664,5.988-13.015,16.206-12.092-47.022-16.973-42.84-9.776,8.358-14.434,71.755-23.4,70.4-12.221-1.847-2.145-61.68-12.832-55.488-42.894,68.7-2.641-95.5-39.853-4.169l-2.987,34.944A12.261,12.261,0,0,0,323.961,362.077Z"
        transform="translate(-102.361 -89.602)"
        opacity="0.75"
        fill="url(#e)"
      />
      <path
        d="M404.269,68.092l37.571,18.046a1.616,1.616,0,0,1,.862,1.473h0c0,6.606-5.866,11.134-11.291,8.724L408.044,85.941a10.291,10.291,0,0,1-5.737-9.5V69.566a1.439,1.439,0,0,1,1.962-1.473Z"
        transform="translate(-129.17 -19.159)"
        fill="#321066"
      />
      <path
        d="M294.821,227.841l87.88,58.143c6.552,4.338,34.408,2.56,35.161-6,0,0,20.5-211.871,19.865-215.958-.435-11.908-7.427-18.779-11.63-20.8C423.177,41.2,341.313,3.274,334,1.271c-9.4-2.716-24.265,1.256-28.3,18.412-4.963,48.407-17.448,193.6-17.448,193.6C287.862,217.874,291.311,225.519,294.821,227.841Z"
        transform="translate(-81.046 25.508)"
        opacity="0.81"
        fill="url(#f)"
      />
      <path
        d="M89.89,700.125V566.48c25.908,14.02,71.585,35.3,160.945,42.29l-1.663,26.9s.984,63.988-13.578,66.025-13.477-79.02-28.121-79.61c-8.52-.346-8.636,57.545-22.269,54.918-14.705-2.831-9.681-64.83-20.952-63.411-17.828,2.24-8.3,41.319-26.234,42.467-9.756.618-11.779-20.137-18.494-20.761-3.945-.367-7.468,26.946-14.692,60.424C104.032,703.425,94.595,717.485,89.89,700.125Z"
        transform="translate(-28.862 -179.213)"
        fill="url(#g)"
      />
      <path
        d="M611.39,239.94v23.828l79.056,44.765,3.247-24.912Z"
        transform="translate(-295.591 -138.446)"
        opacity="0.8"
        fill="url(#h)"
      />
      <path
        d="M611.39,239.94v23.828l79.056,44.765,3.247-24.912Z"
        transform="translate(-295.591 -102.186)"
        opacity="0.8"
        fill="url(#i)"
      />
    </g>
  </svg>
)
