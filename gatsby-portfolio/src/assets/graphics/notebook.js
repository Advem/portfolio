import React from 'react'
export default (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xlink="http://www.w3.org/1999/xlink"
    viewBox="0 0 905.72 735.68"
  >
    <defs>
      <linearGradient
        id="a"
        y1="476.67"
        x2="905.72"
        y2="476.67"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#4e62e4" />
        <stop offset="1" stopColor="#3e007d" />
      </linearGradient>
      <linearGradient
        id="b"
        y1="429.17"
        x2="892.57"
        y2="429.17"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#3cf" />
        <stop offset="1" stopColor="#60c" />
      </linearGradient>
      <linearGradient
        id="c"
        x1="376.43"
        y1="159.53"
        x2="708.89"
        y2="351.47"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#2f67b2" stopOpacity="0.5" />
        <stop offset="0.12" stopColor="#3977b9" stopOpacity="0.58" />
        <stop offset="0.42" stopColor="#509cc8" stopOpacity="0.76" />
        <stop offset="0.67" stopColor="#61b6d4" stopOpacity="0.89" />
        <stop offset="0.88" stopColor="#6bc6da" stopOpacity="0.97" />
        <stop offset="1" stopColor="#6fccdd" />
      </linearGradient>
      <linearGradient
        id="d"
        x1="537.1"
        y1="33.54"
        x2="430.25"
        y2="285.27"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0" stopColor="#fff" stopOpacity="0" />
        <stop offset="0.09" stopColor="#fff" stopOpacity="0.04" />
        <stop offset="0.24" stopColor="#fff" stopOpacity="0.16" />
        <stop offset="0.43" stopColor="#fff" stopOpacity="0.34" />
        <stop offset="0.67" stopColor="#fff" stopOpacity="0.6" />
        <stop offset="0.94" stopColor="#fff" stopOpacity="0.92" />
        <stop offset="1" stopColor="#fff" />
      </linearGradient>
      <linearGradient
        id="e"
        x1="604.48"
        y1="251.9"
        x2="769.29"
        y2="347.05"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0.01" stopColor="#fff" />
        <stop offset="0.58" stopColor="#fff" stopOpacity="0.37" />
        <stop offset="0.91" stopColor="#fff" stopOpacity="0" />
      </linearGradient>
      <linearGradient
        id="f"
        x1="604.48"
        y1="320.56"
        x2="758.63"
        y2="409.55"
        gradientUnits="objectBoundingBox"
      >
        <stop offset="0.01" stopColor="#fff" />
        <stop offset="0.08" stopColor="#fff" stopOpacity="0.96" />
        <stop offset="0.21" stopColor="#fff" stopOpacity="0.87" />
        <stop offset="0.38" stopColor="#fff" stopOpacity="0.71" />
        <stop offset="0.57" stopColor="#fff" stopOpacity="0.5" />
        <stop offset="0.78" stopColor="#fff" stopOpacity="0.22" />
        <stop offset="0.94" stopColor="#fff" stopOpacity="0" />
      </linearGradient>
    </defs>
    <title>Asset 1</title>
    <polygon
      points="573.75 217.67 892.57 360.23 905.73 391.18 784.49 665.81 341.38 735.68 11.38 544.09 0 496.59 129.71 284.89 573.75 217.67"
      fill="url(#a)"
    />
    <polygon
      points="562.37 170.17 892.57 360.23 773.11 618.31 330 688.18 0 496.59 118.33 237.39 562.37 170.17"
      fill="url(#b)"
    />
    <polygon
      points="163.04 428.25 163.04 405.21 381.62 279.29 407.72 55.86 420.78 47.41 708.71 215.56 678.76 468.18 456.86 596.7 163.04 428.25"
      fill="#309"
    />
    <polygon
      points="420.97 76.21 682.03 227.47 658.99 431.7 396.4 279.68 420.97 76.21"
      fill="url(#c)"
    />
    <polygon
      points="381.91 296.95 650.64 451.28 459.45 566.46 190.72 409.05 381.91 296.95"
      fill="#0a003f"
    />
    <polygon
      points="379.31 314.61 619.64 451.28 529.81 508.1 284.87 370.66 379.31 314.61"
      fill="#309"
    />
    <polygon
      points="346.49 422.04 410.22 458.13 360.31 490.38 295.81 456.21 346.49 422.04"
      fill="#309"
    />
    <polygon
      points="398.95 408.94 421.52 420.97 399.86 435.16 377.79 422.8 398.95 408.94"
      fill="#4423ba"
    />
    <polygon
      points="435.62 346.41 457.76 359.21 435.62 372.65 413.99 359.53 435.62 346.41"
      fill="#4423ba"
    />
    <polygon
      points="574.08 425.31 596.34 437.89 574.34 451.54 552.58 438.64 574.08 425.31"
      fill="#4423ba"
    />
    <polygon
      points="307.01 357.47 329.51 369.61 307.78 383.69 284.87 370.66 307.01 357.47"
      fill="#4423ba"
    />
    <polygon
      points="479.38 395.81 501.52 408.61 479.38 422.04 457.76 408.93 479.38 395.81"
      fill="#4423ba"
    />
    <polygon
      points="478.04 453.54 500.66 465.46 479.07 479.76 456.94 467.5 478.04 453.54"
      fill="#4423ba"
    />
    <polygon
      points="426.66 213.64 537.23 278.14 560.26 47.8 447.39 0 426.66 213.64"
      fill="url(#d)"
    />
    <polygon
      points="611.39 239.94 611.39 277.95 737.5 349.36 742.68 309.62 611.39 239.94"
      opacity="0.8"
      fill="url(#e)"
    />
    <polygon
      points="611.39 308.6 611.39 346.61 737.5 418.02 742.68 378.28 611.39 308.6"
      opacity="0.8"
      fill="url(#f)"
    />
  </svg>
)
