import React, { useContext } from 'react'
import { Link, graphql } from 'gatsby'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'

import { StateContext } from '../context/ContextProvider'
import { text } from '../context/langauges'
import { theme, themeBackground } from '../context/theme'
import s from '../styles/project.module.scss'

import Layout from '../components/Layout'
import Head from '../components/Head'

import ImgBlur from '../components/Elements/ImgBlur'

import arrowLeft from '../assets/icons/arrowLeft'
import fileDownload from '../assets/icons/fileDownload'
import gitlab from '../assets/icons/gitlab'
import www from '../assets/icons/www'

export const query = graphql`
  query($slug: String!) {
    english: contentfulProject(slug: { eq: $slug }) {
      slug
      title
      subtitle
      phrase
      color
      tech
      website
      repo
      documentation {
        title
        description
        file {
          url
        }
      }
      cover {
        title
        file {
          url
        }
      }
      date: created(formatString: "Do MMM, YYYY")
      ago: created(fromNow: true, locale: "en-US")
      picture {
        title
        file {
          url
        }
        fluid {
          src
          base64
        }
      }
      shortDescription {
        shortDescription
      }
      description {
        json
      }
    }
    polish: contentfulProject(slug: { eq: $slug }, node_locale: { eq: "pl" }) {
      slug
      title
      subtitle
      phrase
      color
      tech
      website
      repo
      documentation {
        title
        description
        file {
          url
        }
      }
      cover {
        title
        file {
          url
        }
      }
      date: created(formatString: "D MMMM, YYYY", locale: "pl")
      ago: created(fromNow: true, locale: "pl")
      picture {
        title
        file {
          url
        }
        fluid {
          src
          base64
        }
      }
      shortDescription {
        shortDescription
      }
      description {
        json
      }
    }
  }
`

const Project = props => {
  const state = useContext(StateContext)

  const $ = props.data[state.language]
  const styleBackground = {
    background: `linear-gradient(180deg, ${$.color}, rgb(${themeBackground(
      'dark'
    )}))`,
  }
  // -30deg
  const techBackground = {
    backgroundColor: `rgba(${theme('dark')}, 0.15)`,
  }
  const buttonStyle = {
    backgroundColor: `${$.color}80`,
    opacity: '1',
    // border: `2px solid rgba(${theme(state.theme)}, 0.1)`,
    color: `rgba(255, 255, 255, 0.8)`,
    borderRadius: `25px`,
    '&:hover': {
      opacity: '1',
      // border: `2px solid transparent`,
      backgroundColor: `rgba(${themeBackground('dark', true)}, 1)`,
      color: `rgba(${theme('dark', true)}, 1)`,
    },
  }
  const buttonStyleMobile = {
    '@supports (backdrop-filter: blur(10px))': {
      backgroundColor: 'transparent',
      backdropFilter: 'blur(10px)',
    },
  }
  const options = {
    renderNode: {
      'embedded-asset-block': node => {
        const alt = node.data.target.fields.title['en-US']
        const src = node.data.target.fields.file['en-US'].url
        return <img alt={alt} src={src} />
      },
    },
  }

  const renderButton = (text, link, icon) => (
    <a href={`${link}`} target="_blank" rel="noopener noreferrer">
      <div className={s.buttonSmall} css={buttonStyle}>
        {icon}
        <span className={s.buttonText}>{text}</span>
      </div>
    </a>
  )

  const meta = {
    title: $.title && $.subtitle ? `${$.title} - ${$.subtitle}` : 'Project',
    description: $.phrase ? `${$.phrase} | Explore the project now...` : '',
    image: $.cover.file.url ? $.cover.file.url : '/logo512.png',
    theme: state.theme,
    url: $.slug && `/projects/${$.slug}`,
  }

  return (
    <Layout>
      <Head {...meta} />
      <div className={s.container} css={styleBackground}>
        <div className={s.left}>
          <div className={s.text}>
            <h1>{$.title}</h1>
            <h2>{$.subtitle}</h2>
          </div>
          <div className={s.text}>
            <h3>{$.phrase}</h3>
          </div>
          <div className={s.text}>
            {$.shortDescription && (
              <>
                {$.date && (
                  <h5>
                    <span>{text[state.language].published} </span>
                    {$.ago} <span>| {$.date}</span>
                  </h5>
                )}
                <h4>{$.shortDescription.shortDescription}</h4>
              </>
            )}
          </div>
          <div className={s.html}>
            {$.description &&
              documentToReactComponents($.description.json, options)}
          </div>
          {$.tech && (
            <div className={s.text}>
              <p>{text[state.language].technologies}</p>
              <div className={s.techContainer}>
                {$.tech.map((tech, id) => (
                  <div key={id} className={s.tech} css={techBackground}>
                    {tech}
                  </div>
                ))}
              </div>
            </div>
          )}
          <div className={s.buttons} css={buttonStyleMobile}>
            {$.website &&
              renderButton(text[state.language].visit, $.website, www)}
            {$.repo && renderButton(text[state.language].repo, $.repo, gitlab)}
            {$.documentation &&
              renderButton(
                text[state.language].docs,
                $.documentation.file.url,
                fileDownload
              )}
            <Link to={'/#Projects'}>
              <div className={s.buttonSmall} css={buttonStyle}>
                {arrowLeft}
                <span className={s.buttonText}>
                  {text[state.language].projects}
                </span>
              </div>
            </Link>
          </div>
        </div>
        <div className={s.right}>
          {$.picture && (
            <ImgBlur
              url={$.picture.file.url}
              base64={$.picture.fluid.base64}
              alt={$.picture.title}
            />
          )}
        </div>
      </div>
    </Layout>
  )
}

export default Project
